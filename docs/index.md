# ![](assets/postmill-128.png) Postmill documentation

* **[Getting started](../README.md#getting-started)**

* **Configuration & administration**

    * [Database setup](database-setup.md)
    * [Pruning IP addresses automatically](pruning-ips.md)
    * [Deployment][deploy] (external link)
    * [Configuring a web server][web server] (external link)

* **For developers**
    * [Ugly hacks & workarounds](workarounds.md)
    * [Trusted users](trusted_users.md)
   
Something missing? Report it on the [issue tracker][issues] and chances are
we'll add it!


[deploy]: https://symfony.com/doc/3.4/deployment.html
[web server]: https://symfony.com/doc/3.4/setup/web_server_configuration.html
[issues]: https://gitlab.com/edgyemma/Postmill/issues
